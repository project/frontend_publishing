/**
 *
 *
 *
 */
class Frontend_Publisher_Actions {

  /**
   * Initialize
   */
  constructor(settings) {
    this.settings = settings;
  }

  getTransitions(nodeId) {
    return Drupal.restconsumer
      .get('/frontend_publishing/transitions/' + nodeId, false, false).done()
  }

  showTransitionDialog(nodeId, lang, publish) {
    this.getTransitions(nodeId).done(function (response) {
      var transitionSelection = jQuery('#frontendPublishingModal').find('#transition_selection')
      var transitionSelectionTitle = jQuery('#frontendPublishingModal').find('#transition_selection_title');
      transitionSelection.empty();
      if (response.length > 0) {
        for (var x in response) {
          transitionSelection.append('<option value="' + response[x]['id'] + '">' + response[x]['label'] + '</option>');
        }
        transitionSelection.show();
        transitionSelectionTitle.show();
      } else {
        if (publish) {
          transitionSelection.append('<option selected="selected" value="publish">Publish</option>');
        } else {
          transitionSelection.append('<option selected="selected" value="unpublish">Unpublish</option>');
        }
        transitionSelectionTitle.hide();
        transitionSelection.hide();
      }

      jQuery('#frontendPublishingModal').find('#revision_message').val("");
      jQuery('#frontendPublishingModal').dialog({
        resizable: false,
        height: "auto",
        width: 400,
        modal: true,
        buttons: {
          "Publish page": (function (action) {
            return function () {
              jQuery(this).dialog("close");
              var message = jQuery('#frontendPublishingModal').find("#revision_message").val();
              var state = transitionSelection.val();
              Drupal.frontend_publishing.transitionNode(nodeId, lang, state, message);
            }
          })(jQuery(this)),
          Cancel: function () {
            jQuery(this).dialog("close");
          }
        }
      });

    });
  }

  transitionNode(nodeId, lang, state, message) {
    jQuery(window).trigger("frontend_publishing_transition_start", [nodeId, lang, state, message])
    Drupal.restconsumer
      .patch('/frontend_publishing/transitions/' + nodeId, { state: state, language: lang, message: message }, false)
      .done(
        (function (nodeId, lang, state, message) {
          return function (e) {
            jQuery(window).trigger("frontend_publishing_transition_done", [nodeId, lang, state, message])
          }
        })(nodeId, lang, state, message)
      )
      .fail(
        (function (nodeId, lang, state, message) {
          return function (e) {
            jQuery(window).trigger("frontend_publishing_transition_fail", [nodeId, lang, state, message])
          }
        })(nodeId, lang, state, message)
      );
  }
}
