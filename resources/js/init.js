(function ($, Drupal) {
  var initialized;

  $(document).ready(function() {
    $('body').append($('#frontendPublishingModal'));
  });
  Drupal.behaviors.frontend_publishing = {
    attach: function (context, settings) {
      if (!initialized) {
        Drupal.frontend_publishing = new Frontend_Publisher_Actions();
        initialized = true;
      }
    }
  };
})(jQuery, Drupal);