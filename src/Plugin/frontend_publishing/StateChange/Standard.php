<?php

namespace Drupal\frontend_publishing\Plugin\frontend_publishing\StateChange;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\frontend_publishing\Plugin\StateChangePluginBase;

/**
 * Default state change handler.
 *
 * @StateChangeHandler(
 *   id = "standard",
 *   name = @Translation("Default render"),
 *   weight = 0
 * )
 */
class Standard extends StateChangePluginBase {

  /**
   * {@inheritdoc}
   */
  public function publish(ContentEntityBase &$entity) {

    if (!$entity->isPublished()) {
      $entity->setPublished(TRUE);
      $entity->save();
    }
    else {
      Cache::invalidateTags($entity->getCacheTags());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function transition(ContentEntityBase &$entity) {
    $entity->save();
  }

  /**
   * {@inheritdoc}
   */
  public function unpublish(ContentEntityBase &$entity) {
    if ($entity->isPublished()) {
      $entity->setPublished(FALSE);
      $entity->save();
    }
    else {
      Cache::invalidateTags($entity->getCacheTags());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function copy(ContentEntityBase &$entity, ContentEntityBase &$clone = NULL) {
    if ($clone == NULL) {
      $clone = $entity->createDuplicate();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function generate(ContentEntityBase &$entity) {

  }

  /**
   * {@inheritdoc}
   */
  public function delete(ContentEntityBase &$entity) {

  }

}
