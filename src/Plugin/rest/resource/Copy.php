<?php

namespace Drupal\frontend_publishing\Plugin\rest\resource;

use Drupal\rest\ModifiedResourceResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Provides a resource to copy a page and place the new entry in the menu tree.
 *
 * @RestResource(
 *   id = "frontend_publishing_copy",
 *   label = @Translation("iQ page copy"),
 *   uri_paths = {
 *     "https://www.drupal.org/link-relations/create" = "/frontend_publishing/copy"
 *   }
 * )
 */
class Copy extends Move {

  /**
   * Responds to POST requests.
   *
   * Copies a page and it's children under the new parent at the given position.
   * Rebuilds menu.
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   *   The response containing the id of the copy.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   */
  public function post($request) {
    $id = $request['id'];
    $parentId = $request['newParent'];
    $weight = $request['weight'];
    $recursive = $request['recursive'];
    $menu = $request['menu'];
    $entity = $this->getNewestNodeRevision($id);

    if (empty($menu) || $menu == NULL) {
      $menu = 'main';
    }

    if ($entity == NULL) {
      throw new UnprocessableEntityHttpException('Entity not found.');
    }

    if (!$entity->access('create', $this->currentUser)) {
      throw new AccessDeniedHttpException('You are not allowed to create a new node.');
    }

    $clone = $this->stateChange->copy($entity);
    $menuLink = $this->menuHelper::getMenuLink($clone->id(), $menu);
    if ($menuLink == NULL) {
      $menuLink = $this->menuHelper::createMenuLink($clone, $clone->title->value, $parentId, $menu);
    }
    foreach ($entity->getTranslationLanguages(FALSE) as $language) {
      if ($clone->hasTranslation($language->getId()) && !$menuLink->hasTranslation($language->getId())) {
        $menuLink->addTranslation($language->getId(), ['title' => $clone->getTranslation($language->getId())->title->value])->save();
      }
    }

    if ($parentId != -1) {
      $parentLink = $this->menuHelper::loadMenuLink($parentId);
      $menuLink->parent->value = $parentLink->getPluginId();
    }

    if ($menuLink != NULL) {
      $menuLink->weight->value = $weight * 10;
      $menuLink->save();
      $this->menuHelper::reorder($menuLink->parent->value, $menuLink, $menu);
      if ($recursive) {
        $root = $this->menuHelper::getMenuLink($entity->id(), $menu);
        $info = $this->_copyChildren($root, $clone, $menu);
      }
    }

    $url = $clone->toUrl('canonical', ['absolute' => TRUE])->toString(TRUE);
    return new ModifiedResourceResponse(['clone' => $clone->id(), 'original' => $entity->id()], 201, ['Location' => $url->getGeneratedUrl()]);
  }

  /**
   *
   */
  protected function _copyChildren($root, $clone, $menu) {
    if ($root != NULL) {
      $tree = $this->menuHelper::getMenuTree($menu, $root->getPluginId());
      $this->_copyTree($tree, $clone);
    }
    return $tree;
  }

  /**
   *
   */
  protected function _copyTree($tree, $clone) {
    foreach ($tree as $entry) {
      $nid = $entry->link->getRouteParameters()['node'];
      if ($nid != $clone->id()) {
        $entity = $this->getNewestNodeRevision($nid);
        $this->stateChange->copy($entity);
        $this->_copyTree($entry->subtree, $clone);
      }
    }
  }

}
