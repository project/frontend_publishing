<?php

namespace Drupal\frontend_publishing\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase as Base;
use Drupal\node\Entity\Node;

/**
 *
 */
class ResourceBase extends Base {

  /**
   * Returns the node.
   *
   * If the node is moderated, it will automatically fetch the newest revision.
   *
   * @param int $nid
   *   The node id.
   * @return Node|null
   *   The node or null if the node does not exist.
   */
  protected function getNewestNodeRevision($nid) {
    $entity = Node::load($nid);
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('content_moderation') && \Drupal::service('content_moderation.moderation_information')->isModeratedEntity($entity)) {
      $moderationInformation = \Drupal::service('content_moderation.moderation_information');
      $entity = $moderationInformation->getLatestRevision('node', $nid);
    }
    return $entity;
  }

}
