<?php

namespace Drupal\frontend_publishing\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Drupal\node\Entity\Node;

/**
 * Provides a resource to get the avaiable transitions.
 *
 * @RestResource(
 *   id = "frontend_publishing_transitions",
 *   label = @Translation("Get avaiable transitions"),
 *   uri_paths = {
 *     "canonical" = "/frontend_publishing/transitions/{nid}"
 *   }
 * )
 */
class Transitions extends ResourceBase {
  /**
   * The state change service.
   *
   * @var \Drupal\frontend_publishing\Service\StateChange
   */
  protected $stateChange = NULL;

  /**
   * The state change service.
   *
   * @var \Drupal\frontend_publishing\Service\StateChange
   */
  protected $currentUser = NULL;

  /**
   * Constructs a new UnpublishResource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   */
  public function __construct(
        array $configuration,
        $plugin_id,
        $plugin_definition,
        array $serializer_formats,
        LoggerInterface $logger,
        AccountProxyInterface $current_user,
  $state_change) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->stateChange = $state_change;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
          $configuration,
          $plugin_id,
          $plugin_definition,
          $container->getParameter('serializer.formats'),
          $container->get('logger.factory')->get('frontend_publishing'),
          $container->get('current_user'),
          $container->get('frontend_publishing.state_change')
      );
  }

  /**
   *
   *
   * @return \Drupal\rest\ResourceResponse The response containing a list of bundle names.
   */
  public function get($nid = NULL) {
    $entity = $this->getNewestNodeRevision($nid);
    if ($entity == NULL) {
      throw new UnprocessableEntityHttpException('Entity not found.');
    }
    $list = [];
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('content_moderation') && \Drupal::service('content_moderation.moderation_information')->isModeratedEntity($entity)) {
      $transitions = \Drupal::service('content_moderation.state_transition_validation')->getValidTransitions($entity, $this->currentUser);
      foreach ($transitions as $transition) {
        $list[] = ['id' => $transition->to()->id(), 'label' => $transition->to()->label(), 'published' => $transition->to()->isPublishedState(), 'default' => $transition->to()->isDefaultRevisionState()];
      }
    }
    $response = new ResourceResponse($list);
    $response->addCacheableDependency(['cache' => ['max-age' => 0]]);
    return $response;
  }

  /**
   *
   *
   * @return \Drupal\rest\ResourceResponse The response containing a list of bundle names.
   */
  public function patch($nid, $request) {
    $language = $request['language'];
    $message = $request['message'];
    $state = $request['state'];

    $entity = $this->getNewestNodeRevision($nid);
    if ($entity == NULL) {
      throw new UnprocessableEntityHttpException('Entity not found.');
    }

    if (!$entity->access('update', $this->currentUser)) {
      throw new AccessDeniedHttpException('You are not allowed to edit this node.');
    }

    if ($entity->hasTranslation($language)) {
      $entity = $entity->getTranslation($language);
      $results = $this->stateChange->transition($entity, $message, $state);
    }
    else {
      throw new UnprocessableEntityHttpException('The given language is not available on the entity.');
    }
    return new ModifiedResourceResponse($results);
  }

}
