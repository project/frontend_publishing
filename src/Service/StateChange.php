<?php

namespace Drupal\frontend_publishing\Service;

use Drupal\Core\Entity\ContentEntityBase;

/**
 * This service serves as an interface to trigger state changes.
 */
class StateChange {

  /**
   * Publish a node without workflow.
   *
   * Calls the publish method of all state change handler.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The entity being published.
   * @param string $message
   *   The revision log message.
   */
  public function publish(ContentEntityBase &$entity, $message) {
    $entity->setRevisionLogMessage($message);
    $handlers = \Drupal::service('plugin.manager.frontend_publishing_handler')->getHandlers();
    foreach ($handlers as $handler) {
      $handler->publish($entity);
    }
  }

  /**
   * Move a node to another state.
   *
   * Calls the transition method of all state change handler
   * or the publish or unpublish method if no workflow exists.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The entity being moved to another state.
   * @param string $message
   *   The revision message.
   * @param string $state
   *   The target state.
   */
  public function transition(ContentEntityBase &$entity, $message, $state) {
    $entity->setRevisionLogMessage($message);
    $moduleHandler = \Drupal::service('module_handler');
    if ($moduleHandler->moduleExists('content_moderation') && \Drupal::service('content_moderation.moderation_information')->isModeratedEntity($entity)) {
      $entity->moderation_state->value = $state;
      $entity->setNewRevision();
      $handlers = \Drupal::service('plugin.manager.frontend_publishing_handler')->getHandlers();
      foreach ($handlers as $handler) {
        $handler->transition($entity);
      }
    }
    else {
      if ($state == 'publish') {
        $this->publish($entity, $message);
      }
      elseif ($state == 'unpublish') {
        $this->unpublish($entity, $message);
      }
    }
  }

  /**
   * Unpublish a node without workflow.
   *
   * Calls the publish method of all state change handler.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The entity being unpublished.
   * @param string $message
   *   The revision log message.
   */
  public function unpublish(ContentEntityBase &$entity, $message) {
    $entity->setRevisionLogMessage($message);
    $handlers = \Drupal::service('plugin.manager.frontend_publishing_handler')->getHandlers();
    foreach ($handlers as $handler) {
      $handler->unpublish($entity);
    }
  }

  /**
   * Clones a entity and returns the duplicate entity.
   *
   * Clones the base language, creates translations and
   * passes each language version to the registered handlers.
   *
   * @param \Drupal\Core\Entity\ContentEntityBase $entity
   *   The entity to clone.
   *
   * @return \Drupal\Core\Entity\ContentEntityBase
   *   The cloned entity in the default translation.
   */
  public function copy(ContentEntityBase &$entity) {
    $clone = NULL;
    $handlers = \Drupal::service('plugin.manager.frontend_publishing_handler')->getHandlers();
    foreach ($handlers as $handler) {
      $handler->copy($entity, $clone);
    }
    foreach ($clone->getTranslationLanguages(TRUE) as $language) {
      $clone->getTranslation($language->getId())->save();
    }
    $clone = $clone->getUntranslated();
    return $clone;
  }

}
